# Menerapkan aplikasi analisis sentimen di R
# Sekarang, kami akan mencoba menganalisis sentimen tweet yang dibuat oleh gagang Twitter. Kami akan mengembangkan kode dalam R langkah demi langkah dan melihat implementasi praktis dari analisis sentimen dalam R.
# 
# Kode ini dibagi menjadi beberapa bagian berikut:
#   
# Mengekstrak tweet menggunakan aplikasi Twitter
# Membersihkan tweet untuk analisis lebih lanjut
# Mendapatkan skor sentimen untuk setiap tweet
# Memisahkan tweet positif dan negatif

# Kami pertama-tama akan menginstal paket yang relevan yang kami butuhkan. Untuk mengekstrak tweet dari Twitter, kita perlu paket 'twitteR'.
# Paket 'Syuzhet' akan digunakan untuk analisis sentimen; sementara paket 'tm' dan 'SnowballC' digunakan untuk penggalian dan analisis teks.

#  Install Requried Packages
install.packages("SnowballC")
install.packages("tm")
install.packages("twitteR")
install.packages("syuzhet")
install.packages("RCurl")

# Load Requried Packages
library("SnowballC")
library("tm")
library("twitteR")
library("syuzhet")
library("RCurl")


api_key <- "kMslJECsqQqFXt2OczBUDZX1c"
api_secret <- "CW6e436WLfOJO87TB7NnrWZLUjiB2nN2Yz8ntjK31UNOeHA65M"

access_token <- "1141528932975386624-hebEIpBVX29J92Ed0wu5rYAr007njR"
access_token_secret <- "5xxIAQBbHCgAgCzHRVHtTYk2opq0qrhJcORkkNnb2YATU"

setup_twitter_oauth(api_key, api_secret, access_token, access_token_secret)

tweets <- userTimeline("barcelona", n=200,since = "2019-05-01")

n.tweet <- length(tweets)


tweets.df <- twListToDF(tweets) 

head(tweets.df)

head(tweets.df$text)


#MELAKUKAN CLEANING
tweets.df2 <- gsub("http.*","",tweets.df$text)

tweets.df2 <- gsub("https.*","",tweets.df2)

tweets.df2 <- gsub("#.*","",tweets.df2)

tweets.df2 <- gsub("@.*","",tweets.df2)

#HASIL CLEANING DAN TERSISA TEXT SAJA
head(tweets.df2)


#MENDAPATKAN SKOR SENTIMEN UNTUK SETIAP TWEET
word.df <- as.vector(tweets.df2)

emotion.df <- get_nrc_sentiment(word.df)

emotion.df2 <- cbind(tweets.df2, emotion.df) 

head(emotion.df2)

# Output di atas menunjukkan kepada kita berbagai emosi yang ada di setiap tweet. 
# Sekarang, kita akan menggunakan fungsi get_sentiment untuk mengekstraksi skor sentimen untuk setiap tweet.

sent.value <- get_sentiment(word.df)

most.positive <- word.df[sent.value == max(sent.value)]
most.positive 

#Panggil negatif
most.negative <- word.df[sent.value <= min(sent.value)] 
most.negative 

sent.value

#memisahkan positif dan negatif
positive.tweets <- word.df[sent.value > 0]
head(positive.tweets)


negative.tweets <- word.df[sent.value < 0]
head(negative.tweets)


neutral.tweets <- word.df[sent.value == 0]
head(neutral.tweets)

# Alternate way to classify as Positive, Negative or Neutral tweets

category_senti <- ifelse(sent.value < 0, "Negative", ifelse(sent.value > 0, "Positive", "Neutral"))

head(category_senti)

#Jadi, sekarang kami telah menganalisis pegangan twitter Jokowi dan mendapat sentimen di tweet. Terobosannya jumlah total tweet berdasarkan sentimen adalah
table(category_senti)

